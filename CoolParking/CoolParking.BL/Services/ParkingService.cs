﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.


using CoolParking.BL.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

public class ParkingService : IParkingService
{
    private readonly Parking _parking = Parking.getInstance();
    private readonly Object _lockObj = new Object();
    private readonly ILogService _logService;

    public ParkingService(TimerService _withdrawTimer, TimerService _logTimer, ILogService _logService)
    {
        this._logService = _logService;

        _withdrawTimer.Elapsed += (sender, args) =>
        {
            decimal balance = 0;
            foreach (Vehicle item in _parking.Vehicles)
            {
                VehicleType type = item.VehicleType;
                var typeValue = Settings.Tariffs[type];
                balance += typeValue;
                decimal fineCheck = item.Balance - typeValue;

                var check = item.Balance - typeValue;

                if (check < 0 && check * -1 > typeValue)
                {
                    item.Balance = item.Balance - (typeValue * Settings.FineCoefficient);
                }
                else if (check < 0 && (check * -1) < typeValue)
                {
                    item.Balance = 0;
                    item.Balance = item.Balance - ((check * -1) * Settings.FineCoefficient);
                }
                else
                {
                    item.Balance = item.Balance - typeValue;
                }

                TransactionInfo trans = new TransactionInfo();
                trans.Dt = DateTime.Now;
                trans.VehicleId = item.Id;
                trans.Sum = typeValue;
                lock (_lockObj)
                {
                    _parking.Transactions.Add(trans);
                }
            }

            _parking.Balance += balance;
        };

        _logTimer.Elapsed += (sender, args) =>
        {
            StringBuilder line = new StringBuilder();
            lock (_lockObj)
            {
                foreach (var item in _parking.Transactions)
                {
                    line.AppendLine(item.ToString());
                }
                _parking.Transactions.Clear();
            }

            string result = line.ToString();
            _logService.Write(result);
        };
    }

    public void Dispose()
    {
        _parking.Balance = 0;
        lock (_lockObj)
        {
            _parking.Vehicles.Clear();
            _parking.Transactions.Clear();
        }
    }

    public decimal GetBalance()
    {
        return _parking.Balance;
    }

    public int GetCapacity()
    {
        return _parking.Capacity;
    }

    public int GetFreePlaces()
    {
        return _parking.Capacity - _parking.Vehicles.Count;
    }

    public TransactionInfo[] GetLastParkingTransactions()
    {
        return _parking.Transactions.ToArray();
    }

    public ReadOnlyCollection<Vehicle> GetVehicles()
    {
        return _parking.Vehicles.AsReadOnly();
    }

    public string ReadFromLog()
    {
        return _logService.Read();
    }

    public void RemoveVehicle(string vehicleId)
    {
        var itemToRemove = _parking.Vehicles.SingleOrDefault(r => r.Id == vehicleId);
        if (itemToRemove == null)
        {
            throw new ArgumentException();
        }

        _parking.Vehicles.Remove(itemToRemove);
    }

    public void TopUpVehicle(string vehicleId, decimal sum)
    {
        var itemToRemove = _parking.Vehicles.SingleOrDefault(r => r.Id == vehicleId);
        if (itemToRemove == null || sum < 0)
        {
            throw new ArgumentException();
        }

        itemToRemove.Balance += sum;
    }

    public void AddVehicle(Vehicle vehicle)
    {
        var find = _parking.Vehicles.Where(q => q.Id == vehicle.Id).FirstOrDefault();
        if (find != null) { throw new ArgumentException(); }

        _parking.Vehicles.Add(vehicle);
    }
}